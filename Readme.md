# A Generic Process FMEA for the Development of Machine-Learning-based Control Systems

This repository provides files for the overview of results from the generic process FMEA for the development of machine-learning-based control systems.  

For more information see our paper:  
*A Generic Process FMEA for the Development of Machine-Learning-based Control Systems*  
Jass, Philipp and Pereira, Ana and Thomas, Carsten
